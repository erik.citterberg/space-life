#Import Modules
import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (175,238,238)
SEED = ((2, 2), (2, 3), (2,4), (15,14), (15,3), (15,12), (18,22), (18,23), (18,24))
PLAYER = ((10,10),(11,9),(11,10),(11,11))
LIFE = 50
RED = (255,69,0)
ACTIVE = []

def turn(key):
    global PLAYER
    if key == 119:
        NEW_P = []
        for t in PLAYER:
            NEW_P.append(list(t))
        if NEW_P[0][0] < NEW_P[1][0]:
            NEW_P[0][0] -= 1
            NEW_P[1][0] -= 1
            NEW_P[2][0] -= 1
            NEW_P[3][0] -= 1
        else:
            NEW_P[0][0] -= 3
            NEW_P[1][0] -= 1
            NEW_P[2][0] -= 1
            NEW_P[3][0] -= 1
        PLAYER = []
        for l in NEW_P:
            PLAYER.append(tuple(l))
        PLAYER = tuple(PLAYER)
    if key == 115:
        NEW_P = []
        for t in PLAYER:
            NEW_P.append(list(t))
        if NEW_P[0][0] > NEW_P[1][0]:
            NEW_P[0][0] += 1
            NEW_P[1][0] += 1
            NEW_P[2][0] += 1
            NEW_P[3][0] += 1
        else:
            NEW_P[0][0] += 3
            NEW_P[1][0] += 1
            NEW_P[2][0] += 1
            NEW_P[3][0] += 1
        PLAYER = []
        for l in NEW_P:
            PLAYER.append(tuple(l))
        PLAYER = tuple(PLAYER)
    if key == 100:
        NEW_P = []
        for t in PLAYER:
            NEW_P.append(list(t))
        NEW_P[0][1] += 1
        NEW_P[1][1] += 1
        NEW_P[2][1] += 1
        NEW_P[3][1] += 1
        PLAYER = []
        for l in NEW_P:
            PLAYER.append(tuple(l))
        PLAYER = tuple(PLAYER)
    if key == 97:
        NEW_P = []
        for t in PLAYER:
            NEW_P.append(list(t))
        NEW_P[0][1] -= 1
        NEW_P[1][1] -= 1
        NEW_P[2][1] -= 1
        NEW_P[3][1] -= 1
        PLAYER = []
        for l in NEW_P:
            PLAYER.append(tuple(l))
        PLAYER = tuple(PLAYER)
        # w 119 s 115 d 100 a 97

def get_near(coor, grid):
    y, x = coor
    nears = []
    for y_coef in range(-1,2):
        for x_coef in range(-1,2):
            if x+x_coef > -1 and y+y_coef >-1:
                try:
                    if (y,x) == (y + y_coef, x + x_coef):
                        continue
                    nears.append(grid[y+y_coef][x+x_coef])
                except IndexError:
                    pass
    return len([item for item in nears if item])


def its_alive(grid):
    global LIFE
    new_grid = [[0 for col in range(50)] for row in range(50)]
    for y, line in enumerate(grid):
        for x, cell in enumerate(line):
            nears = get_near((y,x), grid)
            if nears < 2 or nears > 3:
                new_grid[y][x] = 0
            else:
                if grid[y][x] == 1:
                    new_grid[y][x] = 1
                    ACTIVE.append((y,x))
                if grid[y][x] == 0 and nears >2:
                    new_grid[y][x] = 1
            if grid[y][x] == 1 and (y, x) in PLAYER:
                LIFE -= 1

    return new_grid

def spaceship(grid, row, column, setup):
    try:
        if setup['glider']:
            if setup['down'] and setup['right']:
                grid[row+2][column] = 1
                grid[row+3][column] = 1
                grid[row+4][column] = 1
                grid[row+4][column - 1] = 1
                grid[row+3][column - 2] = 1
            elif setup['down'] and setup['left']:
                grid[row+2][column] = 1
                grid[row + 3][column] = 1
                grid[row + 4][column] = 1
                grid[row + 4][column + 1] = 1
                grid[row + 3][column + 2] = 1
            elif setup['up'] and setup['left']:
                grid[row-1][column] = 1
                grid[row - 2][column] = 1
                grid[row - 3][column] = 1
                grid[row - 3][column + 1] = 1
                grid[row - 2][column + 2] = 1
            elif setup['up'] and setup['right']:
                grid[row-1][column] = 1
                grid[row - 2][column] = 1
                grid[row - 3][column] = 1
                grid[row - 3][column - 1] = 1
                grid[row - 2][column - 2] = 1
        else:
            grid[row][column -4] = 1
            grid[row][column -3] = 1
            grid[row +1][column -2] = 1
            grid[row -1][column -2] = 1

            grid[row][column -1] = 1
            grid[row][column] = 1
            grid[row][column +1] = 1
            grid[row][column +2] = 1

            grid[row + 1][column +3] = 1
            grid[row - 1][column +3] = 1
            grid[row][column +4] = 1
            grid[row][column +5] = 1


            setup['bomb'] = 0
            setup['glider'] = 1



    except IndexError:
        for x in range(-1,2):
            try:
                grid[row][column+x] = 1
            except IndexError:
                try:
                    grid[row+x][column] = 1
                except IndexError:
                    grid[row][column] = 1
    return grid, setup

def main():
    global PLAYER
    pygame.init()
    MARGIN = 0
    WIDTH = 20
    HEIGHT = 20
    grid = [[0 for col in range(50)] for row in range(50)]
    for cor in SEED:
        grid[cor[0]][cor[1]] = 1
        ACTIVE.append((cor[0], cor[1]))

    WINDOW_SIZE = [1000, 1000]
    pygame.init()
    screen = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("Space life")
    clock = pygame.time.Clock()
    setup = {'up': 0, 'right': 1, 'down': 1, 'left': 0, 'bomb': 0, 'glider': 1}

    done = False
    while not done:
        # Draw the grid
        for row in range(50):
            for column in range(50):
                color = BLACK
                if grid[row][column] == 1:
                    color = WHITE
                if (row, column) in PLAYER:
                    if LIFE > 0:
                        color = BLUE
                    else:
                        color = RED
                pygame.draw.rect(screen,
                                 color,
                                 [(MARGIN + WIDTH) * column + MARGIN,
                                  (MARGIN + HEIGHT) * row + MARGIN,
                                  WIDTH,
                                  HEIGHT])

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
        key = list(pygame.key.get_pressed()).index(1)
        print(key)
        if key == 13:
            row, column = PLAYER[0]
            grid, setup = spaceship(grid, row, column, setup)
        if key == 273:
            setup['up'] = 1
            setup['down'] = 0
        if key == 275:
            setup['right'] = 1
            setup['left'] = 0
        if key == 274:
            setup['up'] = 0
            setup['down'] = 1
        if key == 276:
            setup['left'] = 1
            setup['right'] = 0
        if key == 32:
            setup['bomb'] = 1
            setup['glider'] = 0
        turn(key)
        grid = its_alive(grid)
        pygame.display.flip()
        clock.tick(10)


    pygame.quit()




if __name__ == '__main__':
    main()